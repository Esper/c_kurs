// Felix Bruckmeier, 1529743
// Blatt 2, Aufgabe 8, Parlamentswahl
// C Kurs SS 2015

# include <stdio.h>
# include <stdlib.h>

int main(void) {
    int n, m, G;

    m = 4;          // Number of parties
    double S[m];

    n = 10;         // Number of seats
    G = 200;        // Number of voters
    S[0] = 100;     // Votes for party1
    S[1] = 50;      // Votes for party2
    S[2] = 20;      // Votes for party3
    S[3] = 5;       // Votes for party4

    double r[n][m];
    int maximaCount[m];

    for (int k = 0; k < n; k++) {
        for (int i = 0; i < m; i++) {
            r[k][i] = S[i] / (double) (k + 1);
        }
    }

    for (int i = 0; i < m; ++i) {
        maximaCount[i] = 0;
    }

    double maxima[n][2];
    double currentMaximum = 0;
    int maxK, maxI;

    for (int f = 0; f < n; ++f) {
        for (int k = 0; k < n; ++k) {
            for (int i = 0; i < m; ++i) {
                if (r[k][i] > currentMaximum) {
                    currentMaximum = r[k][i];
                    maxK = k;
                    maxI = i;
                }
            }
        }

        maxima[f][0] = maxK;
        maxima[f][1] = maxI;
        maximaCount[maxI]++;
        currentMaximum = 0;
        r[maxK][maxI] = 0;
    }

    printf("\n");
    for (int i = 0; i < m; ++i) {
        printf("Party %d has %d seats.\n", (i+1), maximaCount[i]);
    }

    return 0;
}

