// Felix Bruckmeier, 1529743
// Blatt 1, Aufgabe 6, Kartenprojektion
// C Kurs SS 2015

# include <stdio.h>
# include <math.h>

double arc(double radians) {
    return radians * M_PI / 180.0;
}

double calcDeltaRad(double phiRad, double lambdaRad) {
    return acos(cos(lambdaRad / 2.0) * cos(phiRad));
}

double calcAlphaRad(double phiRad, double deltaRad) {
    return acos(sin(phiRad) / sin(deltaRad));
}

double calcX(double lambdaDeg, double phiDeg) {
    double lambdaRad, phiRad, alphaRad, deltaRad;
    lambdaRad = arc(lambdaDeg);
    phiRad = arc(phiDeg);
    deltaRad = calcDeltaRad(phiRad, lambdaRad);
    alphaRad = calcAlphaRad(phiRad, deltaRad);

    return (phiRad + deltaRad * cos(alphaRad)) / 2.0;
}

double calcY(double lambdaDeg, double phiDeg) {
    double lambdaRad, phiRad, phi0Rad, alphaRad, deltaRad;
    phi0Rad = arc(40.0);
    lambdaRad = arc(lambdaDeg);
    phiRad = arc(phiDeg);
    deltaRad = calcDeltaRad(phiRad, lambdaRad);
    alphaRad = calcAlphaRad(phiRad, deltaRad);

    return ((lambdaRad * cos(phi0Rad)) + (2.0 * deltaRad * sin(alphaRad))) / 2.0;
}

int main(void) {
    //printf("x: %f  y: %f\n", calcX(60.0, 60.0), calcY(60.0, 60.0));
    //printf("x: %f  y: %f\n", calcX(30.0, 60.0), calcY(30.0, 60.0));
    //printf("x: %f  y: %f\n", calcX(60.0, 30.0), calcY(60.0, 30.0));
    //printf("x: %f  y: %f\n", calcX(70.0, 80.0), calcY(70.0, 80.0));

    double inputLambda, inputPhi;
    while (1) {
        printf("\nEnter lambda: ");
        scanf("%lf", &inputLambda);
        printf("Enter phi: ");
        scanf("%lf", &inputPhi);

        printf("x: %f  y: %f\n\n", calcX(inputLambda, inputPhi), calcY(inputLambda, inputPhi));
    }
}