// Felix Bruckmeier, 1529743
// Blatt 1, Aufgabe 4, Summe und Durchschnitt

# include <stdio.h>
# include <stdlib.h>

int main(int argc, char **argv) {
    float avg;
    long int sum;
    sum = 0;

    if (argc < 2) {
        printf("Please provide natural numbers as commandline arguments.\n");
        exit(0);
    }

    for (int i = 1; i < argc; ++i) {
        sum = sum + strtol(argv[i], NULL, 10);
    }

    printf("sum: %ld\n", sum);

    avg = (float)sum / ((float)(argc - 1));
    printf("avg: %.3f\n", avg);

    return 0;
}