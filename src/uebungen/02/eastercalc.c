// Felix Bruckmeier, 1529743
// Blatt 1, Aufgabe 2, Ostertermin
// C Kurs SS 2015

# include <stdio.h>


void printEaster(int year) {
    int M, N, a, b, c, d, e, dayInMarch, dayInApril, is18thApril;
    M = 15;
    N = 6;

    a = year % 19;
    b = year % 4;
    c = year % 7;
    d = (19 * a + M) % 30;
    e = (2 * b + 4 * c + 6 * d + N) % 7;

    dayInMarch = 22 + d + e;
    dayInApril = d + e - 9;
    is18thApril = d == 28 && e == 6 && (11 * M + 11) % 30 < 19;

    if (dayInMarch <= 31) {
        printf("Easter in year %d is on %dth of March!\n", year, dayInMarch);
    } else {
        switch (dayInApril) {
            case 26:
                printf("Easter in year %d is on 19th of April!\n", year);
                break;
            case 25:
                if (is18thApril) {
                    printf("Easter in year %d is on 18th of April!\n", year);
                    break;
                }
                // else fallthrough!
            default:
                printf("Easter in year %d is on %dth of April!\n", year, dayInApril);
                break;
        }
    }
}

int main(void) {
    printEaster(2015);
    printEaster(2016);
    printEaster(1954);
    printEaster(1965);
    printEaster(1700);
    printEaster(1469);

    return 0;
}
