// Felix Bruckmeier, 1529743
// Blatt 3, Aufgabe 14, Verschlüsselung mit Doppelwürfel
// C Kurs SS 2015

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <ctype.h>


char * readFileContent(char filename[]) {
    FILE *f;
    f = fopen(filename, "r");
    if (!f) {
        printf("Couldn't open file %s!\n", filename);
        exit(0);
    }

    int ch, a, u, l; /* gelesenes Zeichen, allocated, used */
    int n;
    int delta;
    char *w;

    w = (char *) malloc((a = 10) * sizeof(char));
    if (!w) exit(1);

    w[0] = 0;
    u = 0;
    n = 0;
    delta = 10;
    while ((ch = getc(f)) != EOF) {
        if (u + 1 >= a) {
            n++;

            w = (char *) realloc(w, (a += delta) * sizeof(char));
            if (!w) exit(1);

            delta = (int) (delta * 1.2);
        }

        if (isalpha(ch)) {
            w[u++] = tolower(ch);
        } else if (isdigit(ch)) {
            w[u++] = ch;
        }
        w[u] = 0;
    }

    l = strlen(w);

    a = u + 1;
    w = (char *) realloc (w, a * sizeof (char));

    fclose(f);
    return w;
}

void writeToFile(char *filename, char *content) {
    FILE *f;
    f = fopen(filename, "w");
    if (f) {
        fputs(content, f);
    } else {
        printf("Couldn't write file %s\n", filename);
    }
    fclose(f);
}

// from http://stackoverflow.com/a/14911260/1079363
int compare(const void *a, const void *b)
{
    return *(const char *)a - *(const char *)b;
}

int indexOf(const char *string, char c) {
    char *ptr = strchr(string, c);
    if (ptr) {
        return ptr - string;
    } else {
        return -1;
    }
}

int * produceKey(const char *passphrase) {
    int *positions;
    char *passcopy;

    positions = (int *) malloc(strlen(passphrase) * sizeof(int));
    passcopy = (char *) malloc((strlen(passphrase) + 1) * sizeof(char));
    strcpy(passcopy, passphrase);

    qsort(passcopy, strlen(passcopy), 1, compare);

    for (int i = 0; i < strlen(passphrase); i++) {
        positions[i] = indexOf(passcopy, passphrase[i]);
        passcopy[positions[i]] = ' ';
    }

    free(passcopy);
    return positions;
}

char * encryptText(const char *text, const char *passphrase) {
    char **buf;
    char *encText;
    int textlen, passlen, row, col;
    int *key;

    key = produceKey(passphrase);

    textlen = strlen(text);
    passlen = strlen(passphrase);

    buf = (char **) malloc(((textlen / passlen) + 1) * sizeof(char*));
    encText = (char *) malloc((textlen + 1) * sizeof(char));

    row = 0;
    for (int i = 0; i < textlen; i++) {
        col = i % passlen;
        if (col == 0) {
            buf[row] = (char *) malloc(passlen * sizeof(char));
        }

        buf[row][col] = text[i];
        //printf("row: %i col: %i -> %c\n", row, col, text[i]);

        if ((i + 1) % passlen == 0) row ++;
    }

    int rows;
    int k = 0;
    for (int c = 0; c < passlen; c++) {
        col = key[c];
        rows = col < textlen % passlen ? (textlen / passlen) + 1 : (textlen / passlen);

        for (int r = 0; r < rows; r++) {
            //printf("col: %i row: %i -> %c\n", c, r, buf[r][col]);
            encText[k++] = buf[r][col];
        }
    }
    encText[k] = '\0';

    return encText;
}

int main(int argc, char **argv) {
    char *tmp, *src_filename, *dst_filename, *passphrase;

    if (argc != 4) {
        printf("You must supply two filenames and a password as commandline arguments (source, destination, password)!\n");
        exit(0);
    }
    src_filename = argv[1];
    dst_filename = argv[2];
    passphrase = argv[2];

    tmp = readFileContent("text.txt");
    //printf("%s\n", tmp);

    tmp = encryptText(tmp, passphrase);
    //printf("%s\n", tmp);

    writeToFile(dst_filename, tmp);
    printf("The content of %s has been encrypted and stored into %s\n", src_filename, dst_filename);

    return 0;
}
