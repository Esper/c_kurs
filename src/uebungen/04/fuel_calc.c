// Felix Bruckmeier, 1529743
// Blatt 2, Aufgabe 7, Benzinverbrauch
// C Kurs SS 2015

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>

# define SIZE 10000
# define DATA_FILE "fuel.txt"
# define BACKUP_FILE "backup.txt"

struct FuelEntries {
    int numOfEntries;
    int day[SIZE];
    int month[SIZE];
    int year[SIZE];
    double kmTotal[SIZE];
    double refueled[SIZE];
};

struct UserInput {
    int day;
    int month;
    int year;
    double kmTotal;
    double refueled;
};

void recoverBackup() {
    if (rename(BACKUP_FILE, DATA_FILE) == 0) {
        printf("Backup recovered.\n");
    } else {
        printf("Couldn't recover backup.\n");
        exit(1);
    }
}

void readPreviousData(struct FuelEntries *es) {
    int i = 0;
    FILE *f;
    f = fopen(DATA_FILE, "r");

    if (!f) {
        printf("Couldn't read data. Trying to recover from backup...\n");
        recoverBackup();

        // try again
        readPreviousData(es);
        return;
    }

    while (fscanf(f, "%i, %i, %i, %lf, %lf\n", &es->year[i], &es->month[i], &es->day[i], &es->kmTotal[i], &es->refueled[i]) == 5) {
        //printf("read %i: %i, %i, %i, %lf, %lf\n", i, es->year[i], es->month[i], es->day[i], es->kmTotal[i], es->refueled[i]);
        i++;
    }
    es->numOfEntries = i;
    printf("Finished loading %i previous entries.\n", es->numOfEntries);
    fclose(f);
}

void writeBackupFile(struct FuelEntries *es) {
    FILE *f;
    f = fopen(BACKUP_FILE, "w");
    for (int i = 0; i < es->numOfEntries; i++) {
        fprintf(f, "%i, %i, %i, %lf, %lf\n", es->year[i], es->month[i], es->day[i], es->kmTotal[i], es->refueled[i]);
    }
    fclose(f);
}

void appendNewUserInput(struct UserInput *in) {
    FILE *f;
    f = fopen(DATA_FILE, "a");
    fprintf(f, "%i, %i, %i, %lf, %lf\n", in->year, in->month, in->day, in->kmTotal, in->refueled);
    fclose(f);
}

void readUserInput(struct UserInput *in) {
    int r;

    printf("Please enter the current day of the month (dd): ");
    for (r = scanf("%i", &in->day); r != 1 || in->day < 1 || in->day > 31;) {
        printf("Please enter the current day of the month (dd): ");
        r = scanf("%i", &in->day);
    }

    printf("Please enter the current month (mm): ");
    for (r = scanf("%i", &in->month); r != 1 || in->month < 1 || in->month > 12;) {
        printf("Please enter the current month (mm): ");
        r = scanf("%i", &in->month);
    }

    printf("Please enter the current year (yyyy): ");
    for (r = scanf("%i", &in->year); r != 1 || in->year < 1;) {
        printf("Please enter the current year (yyyy): ");
        r = scanf("%i", &in->year);
    }

    printf("Please enter the current kilometers: ");
    for (r = scanf("%lf", &in->kmTotal); r != 1 || in->kmTotal < 1;) {
        printf("Please enter the current kilometers: ");
        r = scanf("%lf", &in->kmTotal);
    }

    printf("Please enter the amount you refueled (liters): ");
    for (r = scanf("%lf", &in->refueled); r != 1 || in->refueled < 1;) {
        printf("Please enter the amount you refueled: ");
        r = scanf("%lf", &in->refueled);
    }
}

double avgFuelConsumption(struct FuelEntries *es, struct UserInput *in) {
    double overallDistance = in->kmTotal - es->kmTotal[0];

    double overallConsumption = in->refueled;
    for (int i = 1; i < es->numOfEntries; i++) {
        overallConsumption += es->refueled[i];
    }

    return overallConsumption / overallDistance * 100;
}

double latestFuelConsumption(struct FuelEntries *es, struct UserInput *in) {
    double distanceSinceLastRefuel = in->kmTotal - es->kmTotal[es->numOfEntries - 1];

    return in->refueled / distanceSinceLastRefuel * 100;
}

void displayFuelConsumption(struct FuelEntries *es, struct UserInput *in) {
    if (es->numOfEntries <= 0) {
        printf("\nCannot display consumption without previous entries!\n");
        return;
    }

    double latestConsumption = latestFuelConsumption(es, in);
    double totalConsumption = avgFuelConsumption(es, in);

    printf("\n");
    printf("Average consumption since last refueling: %.1lf liters per 100km\n", latestConsumption);
    printf("Average consumption overall:              %.1lf liters per 100km\n", totalConsumption);
}

void createFilesInitially() {
    FILE *df;

    if (access(DATA_FILE, F_OK) == -1) {
        if (access(BACKUP_FILE, F_OK) == -1) {
            // both files missing; creating data_file stub
            printf("Creating initial data file...\n");

            df = fopen(DATA_FILE, "w");
            fclose(df);
        }
    }
}

int main(void) {
    struct FuelEntries es;
    struct UserInput input;

    createFilesInitially();

    readPreviousData(&es);
    writeBackupFile(&es);

    readUserInput(&input);

    // sanity check
    if (input.kmTotal < es.kmTotal[es.numOfEntries - 1]) {
        printf("Current kilometers have to be greater than the last time!\n");
        exit(1);
    }

    appendNewUserInput(&input);
    displayFuelConsumption(&es, &input);

    return 0;
}